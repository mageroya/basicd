/*
 * Copyright (c) 2016 Roman Bendt
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 *
 * default unix daemon template using syslog
 *
 * start daemon using 
 * run -d
 * 
 * exit this using 
 * kill -SIGTERM $(pidof runnable)
 *
 */

#include <cstdio>
#include <cstdlib>
#include <csignal>
#include <limits>

#include <string>
#include <vector>
#include <chrono>
#include <thread>
#include <iostream>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>

#include "../../libnih/src/libnih.hpp"

#define DAEMON_NAME "myfirstd"

using std::string;
using std::cout;
using std::endl;
using std::vector;


bool running = true;

void sig_handler(int signum) {
	if (signum == SIGTERM)
		running = false;
}

void process() {
	static int i = 0;
	syslog(LOG_NOTICE, "syslogentry %d", i++);
}

int main(int argc, char *argv[]) {

	vector < string > args(argv, argv + argc);
	bool daemonize = false;

	for (string a : args) {
		if (a == "-d") {
			daemonize = true;
		}
		if (NIH::starts_with(a, "-")) {
			cout << a << endl;
		}
	}

	if (daemonize) {
		setlogmask(LOG_UPTO(LOG_NOTICE));
		openlog(DAEMON_NAME, LOG_CONS | LOG_NDELAY | LOG_PERROR | LOG_PID, LOG_USER);
		syslog(LOG_NOTICE, "dropping to background");

		pid_t pid = fork();
		if (pid < 0) { exit(EXIT_FAILURE); }
		if (pid > 0) { exit(EXIT_SUCCESS); }

		umask(0);
		if (setsid() < 0) { exit(EXIT_FAILURE); }
		if ((chdir("/")) < 0) { exit(EXIT_FAILURE); }
		close(STDOUT_FILENO);
		close(STDIN_FILENO);
		close(STDERR_FILENO);

		if (signal(SIGTERM, sig_handler) == SIG_ERR) {
			syslog(LOG_NOTICE, "failed to register signal\n");
			exit(EXIT_FAILURE);
		}

		int iterations = 0;
		while (running) {
			process();
			std::this_thread::sleep_for(std::chrono::seconds(2));
			if (!(++iterations % 10)) { running = false; }
		}

		syslog(LOG_NOTICE, "exiting daemon");
		closelog();
	}
}

